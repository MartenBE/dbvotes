package dbvotes;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class Dbvotes
{

    private static final int LINEWIDTH = 100;
    private static final String FORMAT_OVERVIEW = "%40s | %-3d" + System.lineSeparator();

    public static void main(String[] args) throws IOException // geen exception handling, bij fout meteen afbreken! (stemmen is te delicaat om met mogelijke fouten verder te werken)
    {
        // Vraag het aantal kandidaten
        int temp = askNumber("Hoeveel kandidaten zijn er?");
        while (temp <= 0)
        {
            System.out.println("Het aantal kandidaten moet een strikt positief getal zijn in het interval ]0, oneindig]");
            temp = askNumber("Hoeveel kandidaten zijn er?");
        }
        final int AMOUNT_OF_CANDIDATES = temp;
        printEmptyLine();

        // Vraag de gegevens van de kandidaten
        ArrayList<Candidate> candidates = new ArrayList<>();
        for (int i = 0; i < AMOUNT_OF_CANDIDATES; i++)
        {
            String name = askString("Geef de kandidaat zijn naam");
            while (!checkIfCancidateNameAvailable(candidates, name))
            {
                name = askString("Deze naam is al gebruikt, geef een andere naam op");
            }

            int votes = askNumber(String.format("Hoeveel stemmen heeft kandidaat \"%s\" gekregen in het db?", name));
            while (votes < 0)
            {
                System.out.println("Het aantal stemmen moet een positief getal zijn in het interval [0, oneindig]");
                votes = askNumber(String.format("Hoeveel stemmen heeft kandidaat \"%s\" gekregen in het db?", name));
            }
            candidates.add(new Candidate(name, votes));
        }
        Collections.sort(candidates);
        printEmptyLine();

        // Geef overzicht stemresultaten
        printLineSeparator('=');
        System.out.println("OVERZICHT STEMRESULTATEN DB:");
        printEmptyLine();

        int sumOfvotes = 0;
        for (Candidate c : candidates)
        {
            sumOfvotes += c.getVotes();
            System.out.format(FORMAT_OVERVIEW, c.getName(), c.getVotes());
        }

        printEmptyLine();
        System.out.format(FORMAT_OVERVIEW, "Totaal", sumOfvotes);
        System.out.println("Noot: onthoudingsstemmen worden totaal genegeerd");

        printEmptyLine();
        printLineSeparator('=');
        printEmptyLine();

        // Vraag het aantal zetels
        temp = askNumber("Hoeveel zetels zijn er?");
        while (temp <= 0)
        {
            System.out.println("Het aantal zetels moet een strikt positief getal zijn in het interval ]0, oneindig]");
            temp = askNumber("Hoeveel zetels zijn er?");
        }
        final int AMOUNT_OF_SEATS = temp;
        printEmptyLine();

        // Berekenen de zetels
        System.out.println("Berekening via de D'Hondt methode:");
        printEmptyLine();

        try
        {
            Map<Candidate, Integer> seats = dhondtMethod(candidates, AMOUNT_OF_SEATS);

            // Geef overzicht zetels
            printLineSeparator('=');
            System.out.println("RESULTATEN ZETELVERDELING D'HONDT METHODE:");
            printEmptyLine();

            for (Map.Entry<Candidate, Integer> e : seats.entrySet())
            {
                Candidate currentCandidate = e.getKey();
                int currentCandidateSeats = e.getValue();
                System.out.format(FORMAT_OVERVIEW, currentCandidate.getName(), currentCandidateSeats);
            }

            printEmptyLine();
            printLineSeparator('=');
        }
        catch (Exception e)
        {
            System.out.println("Er is een fout opgetreden bij het uitvoeren van de D'Hont methode:");
            System.out.println(e.getMessage());
        }
    }

    /*
     * Hulpmethodes berekeningen
     */
    private static boolean checkIfCancidateNameAvailable(ArrayList<Candidate> candidates, String name)
    {
        boolean isNameAlreadyUsed = false;

        int i = 0;
        while (i < candidates.size() && !isNameAlreadyUsed)
        {
            isNameAlreadyUsed = name.equals(candidates.get(i).getName());
            i++;
        }

        return !isNameAlreadyUsed;
    }

    private static Map<Candidate, Integer> dhondtMethod(ArrayList<Candidate> candidates, int amountOfSeats) throws Exception
    {
        if (amountOfSeats <= 0)
        {
            throw new IllegalArgumentException("Het aantal zetels moet een strikt positief getal zijn in het interval ]0, oneindig]");
        }
        else if (candidates.size() <= 0)
        {
            throw new IllegalArgumentException("Het aantal kandidaten moet een strikt positief getal zijn in het interval ]0, oneindig]");
        }

        Map<Candidate, Integer> seats = new TreeMap<>();

        for (Candidate c : candidates)
        {
            seats.put(c, 0);
        }

        for (int i = 1; i <= amountOfSeats; i++)
        {
            Candidate maxCandidate = null;
            double maxAverage = 0;
            for (Map.Entry<Candidate, Integer> e : seats.entrySet())
            {

                double average = ((double) e.getKey().getVotes()) / (e.getValue() + 1);
                if (average > maxAverage || (average == maxAverage && e.getKey().getVotes() > maxCandidate.getVotes()))
                {
                    maxAverage = average;
                    maxCandidate = e.getKey();
                }
            }

            seats.put(maxCandidate, seats.get(maxCandidate) + 1);
            System.out.format(
                    "%4d: %-10.2f | Kandidaat \"%s\" krijgt 1 zetel bij en staat nu op %s zetels" + System.lineSeparator(),
                    i,
                    maxAverage,
                    maxCandidate.getName(),
                    seats.get(maxCandidate)
            );
        }
        printEmptyLine();

        int sumOfSeats = 0;
        for (Map.Entry<Candidate, Integer> e : seats.entrySet())
        {
            sumOfSeats += e.getValue();
        }

        if (sumOfSeats != amountOfSeats)
        {
            throw new Exception("De som van de verdeelde zetels komt niet overeen met het aantal te verdelen zetels");
        }

        return seats;
    }

    /*
     * Hulpmethodes console
     */
    private static void printEmptyLine()
    {
        System.out.println("");
    }

    private static void printLineSeparator(char c)
    {
        for (int i = 0; i < LINEWIDTH - 1; i++)
        {
            System.out.print(c);
        }
        System.out.println(c);
    }

    private static String askString(String prompt) throws IOException
    {
        String answer;

        System.out.print(prompt + ": ");

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in)); // Scanner kan niet overweg met spaties in het antwoord

        answer = br.readLine();

        return answer;
    }

    private static int askNumber(String prompt)
    {
        Scanner sc = new Scanner(System.in);

        System.out.print(prompt + ": ");
        while (!sc.hasNextInt())
        {
            System.out.println("\"" + sc.next() + "\" is geen getal.");
            System.out.print(prompt + ": ");
        }

        return sc.nextInt();
    }
}
