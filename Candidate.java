
package dbvotes;

public class Candidate implements Comparable<Candidate>
{
    String name;
    int votes;

    public Candidate(String name, int votes)
    {
        this.name = name;
        this.votes = votes;
    }

    public String getName()
    {
        return name;
    }

    public int getVotes()
    {
        return votes;
    }

    @Override
    public String toString()
    {
        return String.format("Kandidaat %s heeft %s stemmen", name, votes);
    }

    //
    // compareTo should return 
    // < 0 if this is supposed to be less than other, 
    // > 0 if this is supposed to be greater than other 
    // and 0 if they are supposed to be equal
    //
    @Override
    public int compareTo(Candidate c) {
        return name.compareTo(c.getName());
    } 
}
