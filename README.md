Dit is het programma dat het aantal stemmen van de onafhankelijken in de jeugdraad terugbrengt tot het wettelijk toegelaten aantal stemmen.

Het programma is geschreven in Java, aangezien dit de meest aangeleerde programmeertaal is. Bijna iedereen die met programmeren in contact komt, krijgt Java aangeleerd. Op deze manier kunnen zoveel mogelijk personen de code begrijpen & nalezen en is de transparantie het grootst.

Het programma gebruikt de D'Hondt methode. 
Deze methode wordt ook gebruikt in Belgische en Europese verkiezingen voor de zetelverdeling gebaseerd op het aantal stemmen per partij.

Bron: http://www.verkiezingen.fgov.be/index.php?id=3294&L=1#b

Voorbeelden van de D'Hondt methode (deze kunnen met dit programma nagerekend worden om de juiste werking van het programma te controleren):

- http://www.verkiezingen.fgov.be/index.php?id=3294&L=1#DHONDT
- http://www.senate.be/www/?MIval=/index_senate&MENUID=10000&LANG=nl&PAGE=/newsletter/extern/online/articles/systeem_D_Hondt.html

Net zoals bij de Belgische verkiezingen worden blanco stemmen, ook onthoudingsstemmen genaamd, totaal gegenegeerd.

Bron: http://www.belgium.be/nl/over_belgie/overheid/democratie/verkiezingen/federale_verkiezingen/

De sourcecode is online te vinden op https://bitbucket.org/MartenBE/dbvotes/src